import java.math.BigInteger;
import java.util.InputMismatchException;
import java.util.Scanner;


public class BigIntMulti {

    public static void main(String[] args) {
        //Save data in object
        BigInteger result;

        //Inputs from console
        System.out.println("Enter first number");
        String input1 = "";
        String checkInput1 = checkScan(input1);

        System.out.println("Enter second number");
        String input2 = "";
        String checkInput2 = checkScan(input2);

        /*
        E.g. inputs
        String input1 = "12345678901234567890";
        String input2 = "11111111111111111111";
        */

        //BigInteger converter
        BigInteger first = new BigInteger(checkInput1);
        BigInteger second = new BigInteger(checkInput2);

        //Using the multiply method
        result = first.multiply(second);

        //Showing the result
        System.out.println(first +" * "+second+" =");
        System.out.println(result);

    }


    //IsInteger function
    //Checking all numbers
    private static boolean isInteger(String[] arr) {
        boolean isValidInteger = false;

        for (String x : arr) {
            try {
                Integer.parseInt(x);
                //is Integer
                isValidInteger = true;
            } catch (NumberFormatException ex) {
                System.out.println("Wrong number, try again");
                break;
                //is not Integer
            }
        }
        return isValidInteger;
    }

    //Scan again if it is not a number
    private static String checkScan(String input){
        //Scanner
        Scanner scan = new Scanner(System.in);
        boolean checkArray = false;

        while(!checkArray) {
            //Scan until the number is valid
            input = scan.nextLine();

            //Convert to array
            String[] array;
            array = input.split("");

            //Checking array
            checkArray = isInteger(array);
        }

        return input;
    }


}
